import { User } from "@/types";

const getUsers = async (): Promise<User[]> => {
    const response = await fetch("https://graphqlzero.almansi.me/api", {
        method: "POST",
        headers: {"content-type": "application/json"},
        body: JSON.stringify({
            query: `{
          users {
            data {
              id
              name
               phone
      username
      email
      website
            }
          }
        }`
        })
    });

    const result = await response.json();

return result.data.users.data
}
export default getUsers;